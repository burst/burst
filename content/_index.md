![LOGO](burst_Logo_RGB_UE-14112021.svg "BURST")

## Willkommen!

auf der Website der ReproducibiliTea Gruppe Bielefeld

## Treffen

Das nächste Treffen des Bielefeld University Reproducible Science Tea's ist am Freitag, {{< param nextmeet >}} um 12:00, Vor Ort und Online.

| Datum    | Ort    |      Thema      |
|----------|--------|-----------------|
| {{< param nextmeet >}} | U2-241 | Szollosi, Aba, and Chris Donkin. [Arrested theory development: The misguided distinction between exploratory and confirmatory research.](https://doi.org/10.1177/1745691620966796) Perspectives on Psychological Science 16.4 (2021): 717-724. |

Zoom:
* [Link: Zoom-Meeting beitreten](https://uni-bielefeld.zoom.us/j/94847529577?pwd=b3lSeXEzbDg0RldUOFhJNmF2b1h3Zz09)
* Meeting-ID: 948 4752 9577  
* Passwort: 785514  
  
Wir freuen uns auf Eure Teilnahme!  

## Themen

Themen und Artikel für zukünftige Sitzungen könnt ihr gerne hier vorschlagen:

* [dud-poll.inf.tu-dresden.de/w_w2pLAjIw](https://dud-poll.inf.tu-dresden.de/w_w2pLAjIw/)

## Weitere Infos

* Offizielle [ReproducibiliTea Webseite](https://reproducibilitea.org/journal-clubs/#Bielefeld) mit allen ReproducibiliTeas weltweit
* [GitLab](https://gitlab.ub.uni-bielefeld.de/burst/burst) Repository mit dem Code, der diese Webseite erzeugt
* Unser ReproduciblitiTea im [Vorlesungsverzeichnis (eKVV)](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=466782585) der Uni Bielefeld 

## Kontakt

* [burst@uni-bielefeld.de](mailto:burst@uni-bielefeld.de)

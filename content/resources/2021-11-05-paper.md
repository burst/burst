---
title: "Literatur"
subtitle: ""
tags: [resources]
---

Hier sammeln wir die zu lesende Literatur

| Datum    |      Thema      |  Artikel | Link/DOI |
|----------|-----------------|----------|------|
| 07.03.25 | How to improve psychological theory | Szollosi, A., & Donkin, C. (2021). Arrested theory development: The misguided distinction between exploratory and confirmatory research. Perspectives on Psychological Science, 16(4), 717-724.  | https://doi.org/10.1177/1745691620966796 |
| 07.02.25 | Does psychology have a theory problem? | Scheel et al. (2021). Why hypothesis testers should spend less time testing hypotheses. Perspectives on Psychological Science, 16(4), 744-755. | [10.1177/1745691620966795](https://doi.org/10.1177/1745691620966795) |
| 10.01.25 | Analytical Flexibility | Gould et al. (2023). Same data, different analysts: variation in effect sizes due to analytical decisions in ecology and evolutionary biology | https://ecoevorxiv.org/repository/object/6000/download/12075/ |
| 06.12.24 | How to improve psychological theory | Guest, O. (2024). What makes a good theory, and how do we make a theory good?. Computational Brain & Behavior, 1-15. | https://repository.ubn.ru.nl/bitstream/handle/2066/292762/292762.pdf?sequence=1 |
| 08.11.24 | How to improve psychological theory | Guest, O., & Martin, A. E. (2021). How computational modeling can force theory building in psychological science. Perspectives on Psychological Science, 16(4), 789-802. | [10.1177/1745691620970585](https://doi.org/10.1177/1745691620970585) |
| 12.01.24 | How to improve psychological theory | Borsboom, D., van der Maas, H. L., Dalege, J., Kievit, R. A., & Haig, B. D. (2021). Theory construction methodology: A practical framework for building theories in psychology. Perspectives on Psychological Science, 16(4), 756-766. | [10.1177/1745691620969647](https://doi.org/10.1177/1745691620969647) |
| 06.10.23 | Does psychology have a theory problem? | Scheel et al. (2021). Why hypothesis testers should spend less time testing hypotheses. Perspectives on Psychological Science, 16(4), 744-755. | [10.1177/1745691620966795](https://doi.org/10.1177/1745691620966795) |
| 01.09.23 | What is a theory? | Fried (2020). Theories and models: What they are, what they are for, and what they are about. Psychological Inquiry, 31(4), 336-344. | [10.1080/1047840X.2020.1854011](https://doi.org/10.1080/1047840X.2020.1854011) |
| 07.07.23 | Quantifying Reproducibility | Youyou et al. (2023). A discipline-wide investigation of the replicability of Psychology papers over the past two decades. Proceedings of the National Academy of Sciences, 120(6) | [10.1073/pnas.2208863120](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9963456/pdf/pnas.202208863.pdf) |
| 05.05.23 | Teaching Open Science | Chopik, Bremner, Defever, and Keller (2018). How (and Whether) to Teach Undergraduates About the Replication Crisis in Psychological Science. Teaching of Psychology, 45(2), 158–163. | [10.1177/0098628318762900](https://doi.org/10.1177/0098628318762900) |
| 05.04.23 | Implementing Reproducible Practices | Kathawalla et al. (2021). Easing into open science: A guide for graduate students and their advisors. Collabra: Psychology, 7(1) |  [10.1525/collabra.18684](https://doi.org/10.1525/collabra.18684)|
| 03.03.23 | Implementing Reproducible Practices | Heise et al. (2023) Ten simple rules for implementing open and reproducible research practices after attending a training course. *PLOS Computational Biology*, 19(1), e1010750.  | [10.1371/journal.pcbi.1010750](https://doi.org/10.1371/journal.pcbi.1010750) |
| 27.05.22 | Measurement Crisis | Flake & Fried (2020). Measurement Schmeasurement: Questionable Measurement Practices and How to Avoid them. *Advances in Methods and Practices in Psychological Science*, 3(4), 456-465. | [10.1177/2515245920952393](https://doi.org/10.1177/2515245920952393) |
| 29.04.22 | Analytic Flexibility | Simmons et al. (2011). False-Positive Psychology. *Psychological Science*, 22(11), 1359–1366.  | [10.1177/0956797611417632](https://doi.org/10.1177%2F0956797611417632) |
| 25.03.22 | Reproducibility Now | Open Science Collaboration (2015). Estimating the reproducibility of psychological science. *Science*, 349(6251), aac4716-aac4716 | [10.1126/science.aac4716](https://doi.org/10.1126/science.aac4716) |
| 25.02.22 | Case Study: How to Preregister | -  | https://osf.io/prereg/ ; https://aspredicted.org/ |
| 28.01.22 | Questionable Research Practices | John et al. (2012).  Measuring the Prevalence of Questionable Research Practices With Incentives for Truth Telling. *Psychological Science*, 23(5), 524–532. | [10.1177/0956797611430953](https://doi.org/10.1177/0956797611430953) |
| 10.12.21 | General Overview | Munafò et al. (2017). A manifesto for reproducible science. *Nature human behaviour*, 1(1), 1-9.  | [10.1038/s41562-016-0021](https://pubmed.ncbi.nlm.nih.gov/33954258) |

Weitere Vorschläge sind willkommen!
